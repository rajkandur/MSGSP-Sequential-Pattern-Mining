package data;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Pattern;

import org.apache.commons.collections.map.MultiValueMap;

public class DataParser {

	/**
	 * List of all the sequences in the data file
	 */
	List<Sequence> m_ListofTransactionSeq;
	
	/**
	 * itm and mis key value pair
	 */
	Map<Integer, Double> m_ItmMISMap;
	
	/**
	 * In order to maintain order saving it as separate
	 */
	List<Integer> m_ItemList;
	
	/**
	 * support difference constriant sup(h) - sup(l) <= SDC
	 */
	double SDC = 9999;
	
	/**
	 * Returns the parsed sequence
	 * @return
	 */
	public void parseDataFile(String pathname){
		m_ListofTransactionSeq = new ArrayList<DataParser.Sequence>();
		
		File datafile = new File(pathname);
		try {
			FileReader r = new FileReader(datafile);
			BufferedReader reader = new BufferedReader(r); 
			
			String data = null;
			while( (data = reader.readLine()) != null){
				
				//remove trailing spaces
				data = data.trim();
				
				//<{18, 23, 37, 44}{18, 46}{17, 42, 44, 49}>
				//String regex = "(\\{(\\d+,\\s*)*\\d+\\})+"; This is for entire sequence
				
				String regex = "\\}"; //splitter regex
				Pattern pat =  Pattern.compile(regex);
				data = data.replaceAll("[<>]", "");
				String[] arr = pat.split(data);
				
				//add itm set to sequence
				Sequence seq = new Sequence();
				
				//create itemset and add to sequence
				for(int i = 0; i < arr.length; i++){
					arr[i] = arr[i].replaceAll("\\{", "").trim(); //remove braces => ItemSet
					
					//create a itemset
					ItemSet itmSet = new ItemSet();
					String[] itmArr = arr[i].split(",");
					
					for(int j = 0; j < itmArr.length; j++){
						Item itm = new Item();
						int value = Integer.parseInt(itmArr[j].trim());
						itm.setM_item(value);
						itmSet.addItem(itm);
					}
					
					//add itemset to sequence
					seq.addItmSet(itmSet);
				}
				
				//add sequence to the list
				m_ListofTransactionSeq.add(seq);
			}
			
			System.out.println("Total number of transaction sequences: " + m_ListofTransactionSeq.size());
			System.out.println("------------------------------------------------------------------------");
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	/**
	 * Parsing MIS para file
	 * @param parafile
	 */
	public void parseMIS(String parafile) {
		
		try {
			FileReader r = new FileReader(parafile);
			BufferedReader reader = new BufferedReader(r); 
			m_ItmMISMap = new LinkedHashMap<Integer, Double>();
			
			//temp map aids sorting
			MultiValueMap tMap = new MultiValueMap();
			m_ItemList = new ArrayList<Integer>();
			
			String data = null;
			while( (data = reader.readLine()) != null){
				
				//store support difference constraint
				if(data.contains("SDC")){
					String[] para = data.split("\\s*=\\s*");
					SDC = Double.valueOf(para[1].trim());
					continue;
				}
				
				data = data.trim();
				String[] para = data.split("\\s*=\\s*");
				int itm = Integer.parseInt(para[0].replaceAll("[(MIS)()]", "").trim());
				double mis = Double.parseDouble(para[1].trim());
				
				//insert the Item, MIS value pairs
				tMap.put(mis, itm);
			}
			
			//sort the keys now
			List<Double> misvalues = new ArrayList<Double>(tMap.keySet());
			Collections.sort(misvalues);
			
			//now order the list of items as sorted mis values
			for (Double sortedmis : misvalues) {
				if(tMap.containsKey(sortedmis)){
					Collection<Integer> coll = (List<Integer>) tMap.getCollection(sortedmis);
					if( coll instanceof List){
						for (Integer itm : coll) {
							m_ItmMISMap.put(itm, sortedmis);
							m_ItemList.add(itm);
						}
					}
				}
			}
			
			System.out.println(m_ItemList);
			
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	/**
	 * Get map of MIS and item
	 * @return
	 */
	public Map<Integer, Double> getM_ItmMISMap() {
		return m_ItmMISMap;
	}
	
	/**
	 * Return the item list
	 * @return
	 */
	public List<Integer> getM_ItemList() {
		return m_ItemList;
	}
	
	/**
	 * Get Data sequences
	 * @return
	 */
	public List<Sequence> getM_ListofTransactionSeq() {
		return m_ListofTransactionSeq;
	}

	/**
	 * Returns support difference constraint
	 * @return
	 */
	public double getSDC() {
		return SDC;
	}
	/**
	 * Sequence is list of ItemSet, called elements of a Sequence. s = <a1, a2,... ar> where a1, a2,..ar are elements(Itemsets)
	 * @author kandur
	 *
	 */
	public class Sequence{
		
		List<ItemSet> m_ItmSet = null;
		
		public Sequence() {
			m_ItmSet = new ArrayList<DataParser.ItemSet>();
		}
		
		public List<ItemSet> getM_ItmSet() {
			return m_ItmSet;
		}
		
		public void setM_ItmSet(List<ItemSet> itmSetList) {
			this.m_ItmSet = itmSetList;
		}
		
		public void addItmSet(ItemSet itmSet){
			this.m_ItmSet.add(itmSet);
		}
		
		public void addFrontItmSet(ItemSet itmSet){
			this.m_ItmSet.add(0, itmSet);
		} 
		
		/**
		 * Overridden method
		 */
		public boolean equals(Object obj){
			Sequence s2 = (Sequence) obj;
			
			// null check
			if(this == null || s2 == null)
				return false;
			
			List<ItemSet> listofISs1 = this.m_ItmSet;
			List<ItemSet> listofISs2 = s2.m_ItmSet;
			
			//null check
			if(listofISs1 == null || listofISs2 == null)
				return false;
			
			return listofISs1.equals(listofISs2);
		}
		
		/**
		 * Returns the size of the sequence s
		 * @param s
		 * @return
		 */
		public int getSizeOfSequence(){
			return this == null ? -1 : this.getM_ItmSet().size();
		}
		
		/**
		 * Returns the length of the sequence s
		 * @param s
		 * @return
		 */
		public int getLengthOfSequence(){
			int length = 0;
			List<ItemSet> listIS = this.getM_ItmSet();
			
			//null check
			if(listIS == null)
				return -1;
			
			//empty check
			if(listIS.isEmpty())
				return 0;
			
			for(int i = 0; i < listIS.size(); i++){
				Set<Item> itms = listIS.get(i).getM_ItemSet();
				length += itms.size();
			}
			return length;
		}
			
		/**
		 * containment: should be in the same order
		 * @param subseq
		 * @return
		 */
		public boolean contains(Sequence subseq){
			
			//null check
			if(this == null || subseq == null)
				return false;
			
			//In a relation A contains B, Len(A) must be greater than Len(B)
			if(this.getLengthOfSequence() < subseq.getLengthOfSequence())
				return false;
			
			//check whether they two are equal
			if(this.equals(subseq))
				return true;
			
			List<ItemSet> listofISs1 = this.m_ItmSet;
			List<ItemSet> listofISs2 = subseq.m_ItmSet;
			
			//null check
			if(listofISs1 == null || listofISs2 == null)
				return false;

			//check for containment
			boolean bState = true;
			int searchIndex = 0;
			for(int j = 0; j < listofISs2.size(); j++){
				int pos = findElement(searchIndex, listofISs2.get(j), listofISs1);
				// find the onwards element if search is true for earlier element
				if( pos != -1)
					searchIndex = pos+1; //from next position
				else{
					bState = false;
					break;
				}

			}
			return bState;
		}
	}
	
	private int findElement(int index, ItemSet sis, List<ItemSet> listofItems){
		int pos = -1;
		
		for(int i = index; i < listofItems.size(); i++){
			if(listofItems.get(i).equals(sis) || listofItems.get(i).contains(sis) )
				return i;
		}
		return pos;
	}
	
	/**
	 * Generates k-l subsequences in the same order
	 * @param seq
	 * @return
	 */
	public List<Sequence> generatekminus1Sequence(Sequence seq){
		
		//null check
		if(seq == null)
			return null;
		
		List<Sequence> kminus1_subseqList = new ArrayList<DataParser.Sequence>();
		
		//get all the itemsets
		List<ItemSet> listofISs = seq.getM_ItmSet();
		for (int i = 0; i < listofISs.size(); i++) {
			
			//displayItemSet(listofISs.get(i));
			//System.out.println("\n------------");
			List<ItemSet> subISs = getSubItemSets(listofISs.get(i));
			
			//generate k-1 sub sequences
			if(subISs.isEmpty()){
				Sequence subseq = new Sequence();
				for (int j = 0; j < listofISs.size(); j++) {
					
					//working on i so no need to add it
					if( i == j )
						continue;
					
					//add all the remaining itemsets
					subseq.addItmSet(listofISs.get(j));
				}
				
				//displaySequence(subseq);
				//add to the list of k-1 subsequences
				kminus1_subseqList.add(subseq);
			}
			
			//if not empty, accordingly insert new ItemSets in the place of current ItemSet
			for (ItemSet subIS : subISs) {
				
				//create one subsequence
				Sequence subseq = new Sequence();
				for (int j = 0; j < listofISs.size(); j++) {
					
					//working on i so no need to add it
					if( i == j )
						subseq.addItmSet(subIS);
					else
						subseq.addItmSet(listofISs.get(j));
				}

				//add to the list of k-1 subsequences
				kminus1_subseqList.add(subseq);
				//displaySequence(subseq);
			}	
		}
		return kminus1_subseqList;
	}
	
	/**
	 * Generate k-1 length ItemSets
	 * @param is
	 * @return
	 */
	private List<ItemSet> getSubItemSets(ItemSet inputIS){
		
		List<ItemSet> listOfISs = new ArrayList<DataParser.ItemSet>();
		List<Item> allItemsIns = new ArrayList<DataParser.Item>(inputIS.getM_ItemSet());
		
		int skip = 0;
		ItemSet is = null;
		for(int i = 0; i < allItemsIns.size(); i++){
			
			//adding the item
			if(skip == i){/*skip*/}
			else{
				
				//instantiation
				if(is == null)
					is = new ItemSet();
				
				is.addItem(allItemsIns.get(i));
			}
			
			//adding the sequence and reiterating
			if(i == allItemsIns.size()-1){
				
				//add only valid is
				if(is != null)
					listOfISs.add(is);
				
				//re-initialization
				skip++;
				i = -1;
				is = null;
			}
				
			//exit condition
			if(skip > allItemsIns.size()-1)
					break;
		}

		return listOfISs;
	}
	
	/**
	 * To display list of sequences
	 * @param listOfSeq
	 */
	public void displaySequence(List<Sequence> listOfSeq){

		if(listOfSeq == null)
			return;

		for(int i = 0; i < listOfSeq.size(); i++){
			Sequence seq = listOfSeq.get(i);
			displaySequence(seq);
		}
	}
	
	/**
	 * To display a sequence as <{}{}>
	 * @param seq
	 */
	public void displaySequence(Sequence seq){
		
		//get the sequence and its itemsets(elements)
		List<ItemSet> isList =  seq.getM_ItmSet();
		
		//no need to display empty sequences
		if(isList.isEmpty())
			return;

		System.out.print("<");
		//iterate over all the elements of a sequence
		for(int j = 0; j < isList.size(); j++){
			ItemSet is = isList.get(j);
			displayItemSet(is);
		}
		System.out.println(">");
	}
	
	/**
	 * Cloning sequence
	 * @param seq
	 * @return
	 */
	public Sequence cloneSequence(Sequence seq){
		if(seq == null)
			return null;

		String seqstr = toString(seq);
		return toSequence(seqstr);
	}
	
	/**
	 * ItemSet is set of Items
	 * @author kandur
	 *
	 */
	public class ItemSet{
		
		Set<Item> m_ItemSet = null;
		
		public ItemSet() {
			
			///LinkedHashSet maintains the lexicographic order
			m_ItemSet = new LinkedHashSet<DataParser.Item>();
		}
		
		public Set<Item> getM_ItemSet() {
			return m_ItemSet;
		}
		
		public void setM_ItemSet(Set<Item> itemSet) {
			this.m_ItemSet = itemSet;
		}
		
		public void addItem(Item itm){
			this.m_ItemSet.add(itm);
		}
		
		public void addFrontItem( Item itm )
		{
			Item[] oldSet = new Item[m_ItemSet.size()];
			Iterator<Item> itr = this.m_ItemSet.iterator();
			int ind = 0;
			while(itr.hasNext())
				oldSet[ind++] = itr.next();
			
			this.m_ItemSet.clear();
			this.m_ItemSet.add(itm);
			for (int i=0; i< oldSet.length; i++)
				this.m_ItemSet.add(oldSet[i]);
		}
				
		/**
		 * Equality checking: overrridden method
		 * @param is2
		 * @return
		 */
		public boolean equals(Object obj){
			
			ItemSet is2 = (ItemSet) obj;
			
			//null check
			if(this == null || is2 == null)
				return false;
			
			//if size differs
			if(this.m_ItemSet.size() != is2.m_ItemSet.size())
				return false;
			
			//At this point size of both ItemSet are equal
			Iterator<Item> itr1 = this.m_ItemSet.iterator();
			Iterator<Item> itr2 = is2.m_ItemSet.iterator();
			//check for equality
			while(itr1.hasNext()){
				if(!itr1.next().equals(itr2.next()))
					return false;
				else 
					continue;
			}
			
			//all good
			return true;
		}
		
		/**
		 * Containment checking
		 * @param is2
		 * @return
		 */
		public boolean contains(ItemSet is2){
			
			//null check
			if(this == null || is2 == null)
				return false;

			boolean bState = true;
			int searchIndex = 0;
			List<Item> allItemsIns1 = new ArrayList<DataParser.Item>(this.getM_ItemSet());
			List<Item> allItemsIns2 = new ArrayList<DataParser.Item>(is2.getM_ItemSet());
			
			for(int j = 0; j < allItemsIns2.size(); j++){
				int pos = findItem(searchIndex, allItemsIns2.get(j), allItemsIns1);
				// find the onwards element if search is true for earlier element
				if( pos != -1)
					searchIndex = pos+1; //from next position
				else{
					bState = false;
					break;
				}

			}
			return bState;
		}
	}
	
	private int findItem(int index, Item sitm, List<Item> listofItems){
		int pos = -1;
		
		for(int i = index; i < listofItems.size(); i++){
			if(listofItems.get(i).equals(sitm))
				return i;
		}
		return pos;
	}
	
	/**
	 * To display ItemSet as {}
	 * @param is
	 */
	public void displayItemSet(ItemSet is){

		//get all the items in a itemset
		Set<Item> itms = is.getM_ItemSet();
		Iterator<Item> itr = itms.iterator();

		System.out.print("{");
		while(itr.hasNext()){
			Item itm = itr.next();
			System.out.print(itm.getItem());

			if(itr.hasNext())
				System.out.print(",");
		}
		System.out.print("}");
	}
	
	/**
	 * Individual value/entity of an ItemSet
	 * @author kandur
	 *
	 */
	public class Item{
	
		int m_item = -1;
		
		public Item() {
			
		}
		
		 public Item(int value) {
			this.m_item = value;
		}
		 
		public int getItem() {
			return m_item;
		}
		
		public void setM_item(int value) {
			this.m_item = value;
		}
		
		/**
		 * Equality checking: Overridden method
		 * @param i2
		 * @return
		 */
		public boolean equals(Object obj){
			Item i2 = (Item) obj;
			if(this.m_item == i2.m_item)
				return true;
			
			return false;
		}
	}
	
	/**
	 * Constructs string from sequence
	 * @param seq
	 * @return
	 */
	public String toString(Sequence seq){
		//get the sequence and its itemsets(elements)
		List<ItemSet> isList =  seq.getM_ItmSet();
		String str = new String();
		str = "<";
		//iterate over all the elements of a sequence
		for(int j = 0; j < isList.size(); j++){
			ItemSet is = isList.get(j);

			//get all the items in a itemset
			Set<Item> itms = is.getM_ItemSet();
			Iterator<Item> itr = itms.iterator();

			str += "{";
			while(itr.hasNext()){
				Item itm = itr.next();
				str += itm.getItem();

				if(itr.hasNext())
					str += ",";
			}
			str += "}";
		}
		str += ">";
		
		//System.out.println(str);
		return str;
	}
	
	/**
	 * Constructs Sequence from string
	 * @param seqstr
	 * @return Sequence
	 */
	public Sequence toSequence(String seqstr){
		
		//get the sequence and its itemsets(elements)
		Sequence seq = new Sequence();
		List<ItemSet> isList =  new ArrayList<DataParser.ItemSet>();
				
		for(int i = 0; i < seqstr.length(); i++){
			if( String.valueOf(seqstr.charAt(i)).contains("<") || 
			    String.valueOf(seqstr.charAt(i)).contains(">"))
				continue;
					
			ItemSet is = new ItemSet();
			String temp = new String();
			while( !String.valueOf(seqstr.charAt(i)).contains("}")){
								
				//create item
				if(seqstr.charAt(i) == ','){
					//create and add item to itemset
					Item itm = new Item(Integer.valueOf(temp.trim()));
					is.addItem(itm);
					
					//for new item
					temp = new String();
				}
				else { //append to item
					//skip open brace
					if(!String.valueOf(seqstr.charAt(i)).contains("{"))
						temp += seqstr.charAt(i);
						temp = temp.trim();
				}
				//increment
				i++;
			}
			
			// add last item
			Item itm = new Item(Integer.valueOf(temp));
			is.addItem(itm);
			
			//add a itemset to list of itemset
			isList.add(is);
		}
		
		seq.setM_ItmSet(isList);
		//displaySequence(seq);
		return seq;
	}

	
	public static void main(String[] args){
		DataParser parser = new DataParser();
		//parser.parseDataFile("./data/data.txt");
		//parser.parseMIS("./data/para.txt");
		
		/*Sequence seq1 = parser.new Sequence();
		ItemSet is1 = parser.new ItemSet();
		is1.addItem(parser.new Item(25));
		is1.addItem(parser.new Item(37));
		is1.addItem(parser.new Item(47));
		ItemSet is11 = parser.new ItemSet();
		is11.addItem(parser.new Item(48));
		seq1.addItmSet(is1);
		seq1.addItmSet(is11);
		parser.displaySequence(seq1);
		
		System.out.println("--------------------------------------------");
		
		Sequence seq2 = parser.new Sequence();
		ItemSet is2 = parser.new ItemSet();
		is2.addItem(parser.new Item(1));
		ItemSet is3 = parser.new ItemSet();
		is3.addItem(parser.new Item(2));
		is3.addItem(parser.new Item(3));
		seq2.addItmSet(is2);
		seq2.addItmSet(is3);
		parser.displaySequence(seq2);
		
		Sequence seq3 = parser.new Sequence();
		ItemSet is4 = parser.new ItemSet();
		is4.addItem(parser.new Item(1));
		is4.addItem(parser.new Item(2));
		ItemSet is5 = parser.new ItemSet();
		is5.addItem(parser.new Item(3));
		seq3.addItmSet(is4);
		seq3.addItmSet(is5);
		parser.displaySequence(seq3);*/
		
		/*Sequence seq4 = parser.new Sequence();
		ItemSet is6 = parser.new ItemSet();
		is6.addItem(parser.new Item(46));
		ItemSet is7 = parser.new ItemSet();
		is7.addItem(parser.new Item(18));
		seq4.addItmSet(is6);
		seq4.addItmSet(is7);
		parser.displaySequence(seq4);*/
		
		/*System.out.print("Are seq1 and seq2 equal? => ");
		System.out.print(seq1.equals(seq2));
		System.out.println(seq2.equals(seq1));
		
		System.out.print("Are seq2 and seq3 equal? => ");
		System.out.print(seq2.equals(seq3));
		System.out.println(seq3.equals(seq2));
		
		System.out.print("Are seq1 and seq3 equal? => ");
		System.out.print(seq3.equals(seq1));
		System.out.println(seq1.equals(seq3));*/
		
		/*Sequence seq5 = parser.new Sequence();
		ItemSet is51 = parser.new ItemSet();
		is51.addItem(parser.new Item(18));
		is51.addItem(parser.new Item(23));
		is51.addItem(parser.new Item(37));
		is51.addItem(parser.new Item(44));
		ItemSet is52 = parser.new ItemSet();
		is52.addItem(parser.new Item(18));
		is52.addItem(parser.new Item(46));
		ItemSet is53 = parser.new ItemSet();
		is53.addItem(parser.new Item(17));
		is53.addItem(parser.new Item(42));
		is53.addItem(parser.new Item(44));
		is53.addItem(parser.new Item(49));
		seq5.addItmSet(is51);
		seq5.addItmSet(is52);
		seq5.addItmSet(is53);
		parser.displaySequence(seq5);
		
		//e.g seq1 = <{25, 37, 47}{48}> seq2 = <{37,47}> and <{37}{47}>
		//e.g seq = <{18,23,37,44}{18,46}{17,42,44,49}>
		System.out.print("Does seq5 contains seq4 => ");
		System.out.println(seq5.contains(seq4));	
		
		Sequence seq6 = parser.new Sequence();
		ItemSet is = parser.new ItemSet();
		is.addItem(parser.new Item(36));
		seq6.addItmSet(is);
		
		System.out.println("Length of sequence <{36}>: " + seq6.getLengthOfSequence());*/
		
		//construct and display a sequence
		String A = "<{25,37,47}{48}>";
		Sequence seqA = parser.toSequence(A);
		String B = "<{2}{5}{7}>";
		Sequence seqB = parser.toSequence(B);
		parser.displaySequence(seqB);
		//System.out.println(seqA.contains(seqB));
		/*parser.displaySequence(seqA);
		System.out.println("-----------------");
		parser.displaySequence(parser.generatekminus1Sequence(seqA));*/
	}
}



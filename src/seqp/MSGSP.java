package seqp;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import data.DataParser;
import data.DataParser.Item;
import data.DataParser.ItemSet;
import data.DataParser.Sequence;

public class MSGSP {

	DataParser m_Dparser = null;

	/**
	 * This is only to store single length Item count
	 */
	HashMap<Integer, Integer> m_ItmCountMap = null;

	/**
	 * Store all Fk Sequences
	 */
	HashMap<Integer, List<Sequence>> m_listOfall_k_sequences;

	/**
	 * Invalid value init
	 */
	static int INVALIDMISVALUE = 9999;

	/**
	 * sequential patterns are recorded in this file
	 */
	String m_OutputFileName;

	/**
	 * Default constructor
	 */
	public MSGSP(String datafilePath, String parafilePath, String outfilePath) {

		if(m_Dparser == null)
			m_Dparser = new DataParser();

		//load the data
		m_Dparser.parseDataFile(datafilePath);
		m_Dparser.parseMIS(parafilePath);

		m_OutputFileName = outfilePath;

		//delete if exists
		if(new File(m_OutputFileName).exists())
			new File(m_OutputFileName).delete();

		m_listOfall_k_sequences = new LinkedHashMap<Integer, List<Sequence>>();
	}

	/**
	 * 
	 * @param X
	 * @param Y
	 * @return
	 */
	double getConfidence(double X, double Y){
		double conf = 0.0;
		conf = (X+Y)/X;
		return conf;
	}

	/**
	 *  Returns the support
	 * @param X
	 * @param Y
	 * @param n
	 * @return
	 */
	double getSupportCount(double X, double Y, double n){
		double sup = 0.0;
		sup = (X+Y)/n;
		return sup;
	}

	private double[] getMinSup(){
		double[] m = new double[2];

		//invalid min sup
		if(m_Dparser == null)
			return null;

		//Itm, MIS key value pair
		Map<Integer, Double> itmMISmap = m_Dparser.getM_ItmMISMap();

		//Given MIS is already sorted
		//List<Double> MISList = new ArrayList<Double>(itmMISmap.values());
		//Collections.sort(MISList);

		//get the first element which shall be the minimum
		Set<Integer> keys = itmMISmap.keySet();
		Iterator<Integer> itr = keys.iterator();

		while(itr.hasNext()){
			int item = itr.next();
			int itemCount =  m_ItmCountMap.get(item);
			double sup = getSupportCount(itemCount, 0, m_Dparser.getM_ListofTransactionSeq().size());
			double mis = itmMISmap.get(item); 

			if(sup >= mis){
				m[1] = mis;
				m[0] = item;
				break;
			}
		}

		return m;
	}

	private double[] getMISandItemofSequence(Sequence seq){
		double m[] = new double[2];
		m[0] = INVALIDMISVALUE;
		m[1] = INVALIDMISVALUE;

		//invalid min sup
		if(m_Dparser == null)
			return m;

		List<ItemSet> listofIS = seq.getM_ItmSet();

		//null check
		if(listofIS == null )
			return m;

		//store the MIS of minitem of seq
		for(int i = 0; i < listofIS.size(); i++){

			Set<Item> s1Items = listofIS.get(i).getM_ItemSet();
			Iterator<Item> itr = s1Items.iterator();

			while(itr.hasNext()){

				//get the MIS of the item
				Item itm = itr.next();
				double MISofItm = getMISForItem(itm);

				//Item is not present
				if(MISofItm == -1)
					continue;

				//store the minimum
				if(MISofItm < m[1]){
					m[0] = itm.getItem();
					m[1] = MISofItm;
				}
			}
		}

		return m;
	}

	private int[] getHighestSupandItemofSequence(Sequence seq){
		int m[] = new int[2];
		m[0] = INVALIDMISVALUE;
		m[1] = -1;

		//invalid min sup
		if(m_Dparser == null)
			return m;

		m_Dparser.displaySequence(seq);
		List<ItemSet> listofIS = seq.getM_ItmSet();

		//null check
		if(listofIS == null )
			return m;

		//store the highest count item
		for(int i = 0; i < listofIS.size(); i++){

			Set<Item> s1Items = listofIS.get(i).getM_ItemSet();
			Iterator<Item> itr = s1Items.iterator();

			while(itr.hasNext()){

				//get the MIS of the item
				Item itm = itr.next();
				int countofItm = m_ItmCountMap.get(itm.getItem());

				//Item is not present
				if(countofItm == -1)
					continue;

				//store the maximum
				if(countofItm > m[1]){
					m[0] = itm.getItem();
					m[1] = countofItm;
				}
			}
		}

		return m;
	}

	private int[] getLowestSupandItemofSequence(Sequence seq){
		int m[] = new int[2];
		m[0] = INVALIDMISVALUE;
		m[1] = INVALIDMISVALUE;

		m_Dparser.displaySequence(seq);

		//invalid min sup
		if(m_Dparser == null)
			return m;

		List<ItemSet> listofIS = seq.getM_ItmSet();

		//null check
		if(listofIS == null )
			return m;

		//store the highest count item
		for(int i = 0; i < listofIS.size(); i++){

			Set<Item> s1Items = listofIS.get(i).getM_ItemSet();
			Iterator<Item> itr = s1Items.iterator();

			while(itr.hasNext()){

				//get the MIS of the item
				Item itm = itr.next();
				int countofItm = m_ItmCountMap.get(itm.getItem());

				//Item is not present
				if(countofItm == -1)
					continue;

				//store the minimum
				if(countofItm < m[1]){
					m[0] = itm.getItem();
					m[1] = countofItm;
				}
			}
		}

		return m;
	}

	private double[] getHighestMISandItemofSequence(Sequence seq){
		double m[] = new double[2];
		m[0] = INVALIDMISVALUE;
		m[1] = -1;

		//invalid min sup
		if(m_Dparser == null)
			return m;

		List<ItemSet> listofIS = seq.getM_ItmSet();

		//null check
		if(listofIS == null )
			return m;

		//store the MIS of minitem of seq
		for(int i = 0; i < listofIS.size(); i++){

			Set<Item> s1Items = listofIS.get(i).getM_ItemSet();
			Iterator<Item> itr = s1Items.iterator();

			while(itr.hasNext()){

				//get the MIS of the item
				Item itm = itr.next();
				double MISofItm = getMISForItem(itm);

				//Item is not present
				if(MISofItm == -1)
					continue;

				//store the minimum
				if(MISofItm > m[1]){
					m[0] = itm.getItem();
					m[1] = MISofItm;
				}
			}
		}

		return m;
	}


	/**
	 * Generate L for generation of C2
	 * @param listOfSeq
	 * @return
	 */
	private List<Integer> initpass(){

		List<Integer> L = new ArrayList<Integer>();

		//get the transaction seq
		List<Sequence> listOfSeq = m_Dparser.getM_ListofTransactionSeq();

		//save the counts
		m_ItmCountMap = new LinkedHashMap<Integer, Integer>();

		for(int i = 0; i < listOfSeq.size(); i++){
			//get the sequence and its itemsets(elements)
			Sequence seq = listOfSeq.get(i);
			List<ItemSet> isList =  seq.getM_ItmSet();

			//iterate over all the elements of a sequence
			Set<Integer> itms = new HashSet<Integer>();
			for(int j = 0; j < isList.size(); j++){
				ItemSet is = isList.get(j);

				//get all the items in a itemset

				//duplicate count in a sequence is not considered
				if(j != isList.size()-1){
					List<Item> im = new ArrayList<DataParser.Item>(is.getM_ItemSet());
					for(int k = 0; k < im.size(); k++){
						itms.add(im.get(k).getItem());
					}
					continue;
				}

				//only on last iteration //last itemset
				List<Item> im = new ArrayList<DataParser.Item>(is.getM_ItemSet());
				for(int k = 0; k < im.size(); k++){
					itms.add(im.get(k).getItem());
				} 

				//iterate
				Iterator<Integer> itr = itms.iterator();
				while(itr.hasNext()){
					int itmValue = itr.next();
					// to make single count of repetitive numbers in the same sequence
					if(m_ItmCountMap.containsKey(itmValue)) 
						m_ItmCountMap.put(itmValue, m_ItmCountMap.get(itmValue)+1);
					else{
						m_ItmCountMap.put(itmValue, 1);
					}
				}
			}
		}

		//if the items are not present init the count to zero
		List<Integer> all = m_Dparser.getM_ItemList();
		for (Integer itm : all) {
			if(!m_ItmCountMap.containsKey(itm))
				m_ItmCountMap.put(itm, 0);
		}

		//sort and get the min sup
		double[] m = getMinSup();

		//only insert Item which has support count >= minsup
		boolean bFound = false;
		for (Integer key : all) {

			//start from first minsup item
			if(key == m[0])
				bFound = true;

			if(!bFound)
				continue;

			//get the count and calculate support
			int count = m_ItmCountMap.get(key);
			//System.out.println(key + ": " + count);
			double sup = getSupportCount(count, 0, listOfSeq.size());
			if( sup >= m[1]){
				L.add(key);
			}
		}

		System.out.println("\nL: " + L);
		return L;
	}

	/**
	 * Return Frequent 1-sequence pattern
	 * @return
	 */
	private List<Sequence> generateF1(List<Integer> L){

		List<Sequence> listOfF1 = new ArrayList<DataParser.Sequence>();
		List<Sequence> listOfSeq = m_Dparser.getM_ListofTransactionSeq();

		//iterate over the counts
		if(m_ItmCountMap != null){

			Set<Integer> all = m_Dparser.getM_ItmMISMap().keySet();
			Iterator<Integer> allItr = all.iterator();
			String displayString = new String();

			while(allItr.hasNext()){
				int key = allItr.next();

				//get the count and calculate support
				int count = m_ItmCountMap.get(key);
				double sup = getSupportCount(count, 0, listOfSeq.size());

				//Item belongs to L and sup >= MIS(Item)
				if(L.contains(key) && sup >= m_Dparser.getM_ItmMISMap().get(key)){
					ItemSet is = m_Dparser.new ItemSet();
					is.addItem(m_Dparser.new Item(key));
					Sequence seq = m_Dparser.new Sequence();
					seq.addItmSet(is);

					//add to the list
					listOfF1.add(seq);

					displayString += "Pattern: " + m_Dparser.toString(seq) + " Count: " + count + "\n";
				}
			}

			//display the 1-sequential pattern
			writeOutput(String.valueOf(listOfF1.size()) + "\n");
			writeOutput(displayString + "\n");
		}		

		// adding to the list of k-sequences
		m_listOfall_k_sequences.put(1, listOfF1);

		//displayF1
		//m_Dparser.displaySequence(listOfF1);
		//return F1 sequential pattern
		return listOfF1;
	}

	private List<Sequence> level2CandidateGenSPM(List<Integer> L){
		List<Sequence> C2 = new ArrayList<DataParser.Sequence>();

		for(int i = 0; i < L.size(); i++){
			int itm1 = L.get(i);
			int itm1Count = m_ItmCountMap.get(itm1);
			//if(itm1 == 33)
			//System.out.println(itm1Count);

			float itm1Sup = (float)getSupportCount(itm1Count, 0, m_Dparser.getM_ListofTransactionSeq().size());
			float itm1MIS = m_Dparser.getM_ItmMISMap().get(itm1).floatValue();


			for(int j = 0; j < L.size();j++){
				int itm2 = L.get(j);
				int itm2Count = m_ItmCountMap.get(itm2);
				float itm2Sup = (float)getSupportCount(itm2Count, 0, m_Dparser.getM_ListofTransactionSeq().size());
				float itm2MIS = m_Dparser.getM_ItmMISMap().get(itm2).floatValue();

				/*if(itm1 == 33 && itm2 == 4){
						System.out.println(itm1Sup);
						System.out.println(m_Dparser.getM_ItmMISMap().get(itm1));
						System.out.println(itm2Sup);
						System.out.println(m_Dparser.getM_ItmMISMap().get(itm2));
					}*/

				float SDC = (float) m_Dparser.getSDC();
				float supdiff = (float)Math.abs(itm2Count - itm1Count)/ (float)m_Dparser.getM_ListofTransactionSeq().size();
				
				//lexicographic order has to maintain in an element of a sequence
				if( (float)itm2MIS >= (float)itm1MIS && (float)itm2Sup >= m_Dparser.getM_ItmMISMap().get(itm1) && /*SDC check*/
						((float)supdiff <= (float)SDC) ){

					ItemSet is = m_Dparser.new ItemSet();
					is.addItem(m_Dparser.new Item(itm1));
					is.addItem(m_Dparser.new Item(itm2));

					//add the itemset to seq
					Sequence seq = m_Dparser.new Sequence();
					seq.addItmSet(is);
					C2.add(seq);
				}

				///different elements so no need to check the order
				//SDC check and MIS checking				float seqMIS = (float)itm1MIS <= (float)itm2MIS ? (float)itm1MIS : (float)itm2MIS;
					if((float)supdiff <= (float)SDC && (float)itm1Sup >= (float)seqMIS && (float)itm2Sup >= (float)seqMIS){
					//second type of itemset
					ItemSet is21 = m_Dparser.new ItemSet();
					is21.addItem(m_Dparser.new Item(itm1));
					ItemSet is22 = m_Dparser.new ItemSet();
					is22.addItem(m_Dparser.new Item(itm2));

					//add the itemset to seq2
					Sequence seq2 = m_Dparser.new Sequence();
					seq2.addItmSet(is21);
					seq2.addItmSet(is22);

					C2.add(seq2);
				}
			}
		}

		//System.out.println("C2 without pruning..");
		//m_Dparser.displaySequence(C2);

		//prune the sequence: No pruning for level 2
		//System.out.println("C2 after pruning..");
		//List<Sequence> C2AP = pruneSequence(1, C2);
		//m_Dparser.displaySequence(C2AP);
		//String seqstr = "<{33}{4}>";
		//C2.add(m_Dparser.toSequence(seqstr));
		return C2;
	}

	/**
	 * To check whether MIS(First Item) < MIS(All other Items) in sequence s
	 * @param s
	 * @return
	 */
	private boolean FitemLessThanOth(Sequence s){
		//get the first item
		Item first = getItem(s, 1); //obv we do not have empty itemset

		List<ItemSet> listOfImst = s.getM_ItmSet();
		for(int i = 0; i < listOfImst.size(); i++){
			ItemSet is = listOfImst.get(i);
			Set<Item> itms = is.getM_ItemSet();
			Iterator<Item> itr = itms.iterator();

			while(itr.hasNext()){
				Item itm = itr.next();
				if( m_Dparser.getM_ItmMISMap().get(first.getItem()) > 
				m_Dparser.getM_ItmMISMap().get(itm.getItem()))
					return false;
			}
		}

		return true;
	}

	/**
	 * To check whether MIS(Last Item) < MIS(All other Items) in sequence s
	 * @param s
	 * @return
	 */
	private boolean LitemLessThanOth(Sequence s){
		//get the last item
		Item last = getItem(s, s.getLengthOfSequence()); //last item in the sequence

		List<ItemSet> listOfImst = s.getM_ItmSet();
		for(int i = 0; i < listOfImst.size(); i++){
			ItemSet is = listOfImst.get(i);
			Set<Item> itms = is.getM_ItemSet();
			Iterator<Item> itr = itms.iterator();

			while(itr.hasNext()){
				Item itm = itr.next();

				//no need to compare against last item
				if(!itr.hasNext() && i == listOfImst.size()-1)
					continue;

				//compare with all the items
				System.out.println("last: " + last.getItem());
				if( m_Dparser.getM_ItmMISMap().get(last.getItem()) > 
				m_Dparser.getM_ItmMISMap().get(itm.getItem()) )
					return false;
			}
		}

		return true;
	}

	/**
	 * Drops the Item specified by place holder and returns new subsequence formed
	 * @param s
	 * @param itemPlaceholder
	 * @return Sub sequence
	 */
	private Sequence dropItem(Sequence seq, int itemPlaceholder){
		int count = 0;
		int prevCount = 0;

		//m_Dparser.displaySequence(seq);
		List<ItemSet> listIS = seq.getM_ItmSet();
		List<Item> itms;
		Sequence subseq = m_Dparser.new Sequence();
		boolean bRemovedStatus = false;
		for(int i = 0; i < listIS.size(); i++){
			itms = new ArrayList<DataParser.Item>(listIS.get(i).getM_ItemSet());
			count += itms.size();

			if(bRemovedStatus){
				ItemSet is = m_Dparser.new ItemSet();
				for(int j = 0; j < itms.size(); j++)
					is.addItem(itms.get(j));

				subseq.addItmSet(is);
				//m_Dparser.displaySequence(subseq);
			}

			//check whether to delete in this itemset
			else if( count < itemPlaceholder){
				prevCount = count;
				ItemSet is = m_Dparser.new ItemSet();
				for(int j = 0; j < itms.size(); j++)
					is.addItem(itms.get(j));

				subseq.addItmSet(is);
				//m_Dparser.displaySequence(subseq);
				continue;
			}
			else if( count == itemPlaceholder){
				int newpholder = itms.size() - 1/*0 indexed*/;
				ItemSet is = m_Dparser.new ItemSet();
				for(int j = 0; j < itms.size(); j++){
					if(j != newpholder)
						is.addItem(itms.get(j));
				}

				//add to the subseq
				if(is.getM_ItemSet().size() > 0)
					subseq.addItmSet(is);

				bRemovedStatus = true;
			}
			else { // placeholder is in somewhere inside in the itemset
				int newpholder = itemPlaceholder - prevCount - 1/*0 indexed*/; 
				ItemSet is = m_Dparser.new ItemSet();
				for(int j = 0; j < itms.size(); j++){
					if(j != newpholder)
						is.addItem(itms.get(j));
				}

				//add to the subseq
				if(is.getM_ItemSet().size() > 0)
					subseq.addItmSet(is);

				bRemovedStatus = true;
			}
		}

		//m_Dparser.displaySequence(subseq);
		return subseq;
	}

	/**
	 * To get MIS for any Item present in data
	 * @param itm
	 * @return
	 */
	public double getMISForItem(Item itm){

		if(m_Dparser.getM_ItmMISMap().containsKey(itm.getItem()))
			return m_Dparser.getM_ItmMISMap().get(itm.getItem());

		return -1;
	}

	/**
	 * 
	 * @param s
	 * @param itemPlaceholder 1: first item, 2: second item and goes on.. -1 denotes last time
	 * @return
	 */
	private Item getItem(Sequence s, int itemPlaceholder){
		int count = 0;
		List<ItemSet> listIS = s.getM_ItmSet();
		for(int i = 0; i < listIS.size(); i++){
			Set<Item> itms = listIS.get(i).getM_ItemSet();
			Iterator<Item> itr = itms.iterator();
			while(itr.hasNext()){
				count++;
				Item itm = itr.next();

				//last item in the sequence
				if(count == -1){
					if(i == listIS.size()-1 && !itr.hasNext())
						return itm;
				}
				else if(count == itemPlaceholder){
					return itm;
				}
			}	
		}
		return null;
	}

	/**
	 * 
	 * @param seq1
	 * @param seq2
	 * @return candidate sequences
	 */
	private List<Sequence> doMSCandidateJoin1(Sequence seq1, Sequence seq2){
		List<Sequence> Ck = new ArrayList<DataParser.Sequence>();

		//(1)
		Sequence subseq1 = dropItem(seq1, 2);
		//m_Dparser.displaySequence(subseq1);
		Sequence subseq2 = dropItem(seq2, seq2.getLengthOfSequence());
		//m_Dparser.displaySequence(subseq2);

		//m_Dparser.displaySequence(seq1);
		//m_Dparser.displaySequence(seq2);

		//(2)
		Item lastItemofseq2 = getItem(seq2, seq2.getLengthOfSequence());
		//System.out.println(lastItemofseq2.getItem());
		double lastItemofseq2MIS = getMISForItem(lastItemofseq2);
		Item firstItemofseq1 = getItem(seq1, 1);
		//System.out.println(firstItemofseq1.getItem());
		double firstItemofseq1MIS = getMISForItem(firstItemofseq1);

		//extending s1 with last item of s2 
		float subseqMIS = (float) getMISandItemofSequence(subseq2)[1];
		boolean bSameMISforAll = (float)subseqMIS == (float)lastItemofseq2MIS ? 
								 ((float)lastItemofseq2MIS == (float)firstItemofseq1MIS): false;	
		if(subseq1.equals(subseq2) && ( ((float)lastItemofseq2MIS > (float)firstItemofseq1MIS) || bSameMISforAll) ){

			//if last item 'l' in s2 is a separate element
			ItemSet lastIS = seq2.getM_ItmSet().get(seq2.getM_ItmSet().size()-1);
			int sizeofs1 = seq1.getSizeOfSequence();
			int lenofs1 = seq1.getLengthOfSequence();
			Item lastItemofseq1 = getItem(seq1, seq1.getLengthOfSequence());

			//maintain lexicographic order
			/*int lastItemofseq2index = m_Dparser.getM_ItemList().indexOf(lastItemofseq2.getItem());
			int lastItemofseq1index = m_Dparser.getM_ItemList().indexOf(lastItemofseq1.getItem());*/

			//float lastItemofseq2MIS = (float) getMISForItem(lastItemofseq2);
			float lastItemofseq1MIS = (float) getMISForItem(lastItemofseq1);

			if(lastIS.getM_ItemSet().size() == 1 /*&& lastIS.getM_ItemSet().equals(lastItemofseq2)*/){

				//first candidate sequence

				Sequence c1 = m_Dparser.cloneSequence(seq1);  //(Sequence) seq1.clone();
				//m_Dparser.displaySequence(c1);
				ItemSet newis = m_Dparser.new ItemSet();
				newis.addItem(m_Dparser.new Item(lastItemofseq2.getItem()));
				//m_Dparser.displaySequence(c1);
				c1.addItmSet(newis);
				//m_Dparser.displaySequence(c1);
				//System.out.println("11");
				//add the candidate sequence
				Ck.add(c1);

				//len and size of s1 is 2 and last item of s2 > last item of s1: so index should be less as it is increasing 
				//order
				if(sizeofs1 == 2 && lenofs1 == 2 && ((float)lastItemofseq2MIS >= (float)lastItemofseq1MIS)){

					//second candidate sequence
					Sequence c2 = m_Dparser.cloneSequence(seq1);
					//m_Dparser.displaySequence(c2);
					ItemSet oldis = c2.getM_ItmSet().get(c2.getM_ItmSet().size()-1); //last itemset
					oldis.addItem(m_Dparser.new Item(lastItemofseq2.getItem()));
					//m_Dparser.displaySequence(c2);
					//System.out.println("12");

					//add the candidate sequence
					Ck.add(c2);
				}
			}


			else if( (sizeofs1 == 1 && lenofs1 == 2 && ((float)lastItemofseq2MIS >= (float)lastItemofseq1MIS) ) ||
					lenofs1 > 2){

				//second candidate sequence
				Sequence c2 = m_Dparser.cloneSequence(seq1);
				//m_Dparser.displaySequence(c2);
				ItemSet oldis = c2.getM_ItmSet().get(c2.getM_ItmSet().size()-1); //last itemset
				oldis.addItem(m_Dparser.new Item(lastItemofseq2.getItem()));
				//m_Dparser.displaySequence(c2);
				//m_Dparser.displaySequence(seq1);
				//System.out.println("13");

				//add the candidate sequence
				Ck.add(c2);
			}
		}

		//m_Dparser.displaySequence(Ck);
		return Ck;
	}

	private List<Sequence> doMSCandidateJoin2(Sequence seq1, Sequence seq2){
		List<Sequence> Ck = new ArrayList<DataParser.Sequence>();

		//(1)
		Sequence subseq1 = dropItem(seq1, 1);
		Sequence subseq2 = dropItem(seq2, seq2.getLengthOfSequence()-1);
		//(2)
		Item lastItemofseq2 = getItem(seq2, seq2.getLengthOfSequence());
		double lastItemofseq2MIS = getMISForItem(lastItemofseq2);
		Item firstItemofseq1 = getItem(seq1, 1);
		double firstItemofseq1MIS = getMISForItem(firstItemofseq1);
		Item firstItemofseq2 = getItem(seq2, 1);

		//maintain lexicographic order
		/*int firstItemofseq2index = m_Dparser.getM_ItemList().indexOf(firstItemofseq2.getItem());
		int firstItemofseq1index = m_Dparser.getM_ItemList().indexOf(firstItemofseq1.getItem());
		 */
		float firstItemofseq2MIS = (float) getMISForItem(firstItemofseq2);

		//extending s2 with first item of s1 
		float subseqMIS = (float) getMISandItemofSequence(subseq2)[1];
		boolean bSameMISforAll = (float)subseqMIS == (float)lastItemofseq2MIS ? 
								 ((float)lastItemofseq2MIS == (float)firstItemofseq1MIS): false;
		if(subseq1.equals(subseq2) && ( ((float)lastItemofseq2MIS < (float)firstItemofseq1MIS)) || bSameMISforAll ){

			//if first item 'f' in s1 is a separate element
			ItemSet firstIS = seq1.getM_ItmSet().get(1);
			int sizeofs2 = seq2.getSizeOfSequence();
			int lenofs2 = seq2.getLengthOfSequence();

			if(firstIS.getM_ItemSet().size() == 1 /*&& firstIS.getM_ItemSet().equals(firstItemofseq1)*/){

				//first candidate sequence
				Sequence c1 = m_Dparser.cloneSequence(seq2);
				ItemSet newis = m_Dparser.new ItemSet();
				newis.addItem(m_Dparser.new Item(firstItemofseq1.getItem()));
				c1.addFrontItmSet(newis); // add to the front of sequence
				//m_Dparser.displaySequence(c1);
				//System.out.println("21");

				//add the candidate sequence
				Ck.add(c1);
				m_Dparser.displaySequence(c1);

				//len and size of s2 are 2 and first item of s1 < first item of s2:so index should be greater as it is increasing 
				//order
				if(sizeofs2 == 2 && lenofs2 == 2 && ((float)firstItemofseq1MIS <= (float)firstItemofseq2MIS)){

					//second candidate sequence
					Sequence c2 = m_Dparser.cloneSequence(seq2);
					ItemSet oldis = c2.getM_ItmSet().get(1); //first itemset of s2
					oldis.addFrontItem(m_Dparser.new Item(firstItemofseq1.getItem())); // add to the front of set

					// add to the front of sequence
					//m_Dparser.displaySequence(c2);
					//System.out.println("22");
					//add the candidate sequence
					Ck.add(c2);
					m_Dparser.displaySequence(c2);
				}
			}

			else if( ( (sizeofs2 == 1 && lenofs2 == 2) &&  ((float)firstItemofseq1MIS <= (float)firstItemofseq2MIS) ) 
					|| (lenofs2 > 2) )
			{


				//second candidate sequence
				Sequence c2 = m_Dparser.cloneSequence(seq2);
				ItemSet oldis = c2.getM_ItmSet().get(1); //first itemset of s2
				oldis.addFrontItem(m_Dparser.new Item(firstItemofseq1.getItem())); // add to the front of set

				//m_Dparser.displaySequence(c2);
				//System.out.println("23");
				//add the candidate sequence
				Ck.add(c2);
				m_Dparser.displaySequence(c2);
			}
		}
		return Ck;

	}
	/**
	 * Join and Pruning for all the candidate sequences of length greater than 2.
	 * @param k
	 * @param Fk_minus_1
	 * @return
	 */
	private List<Sequence> MSCandidateGenSPM(int k, List<Sequence> Fk_minus_1) {
		List<Sequence> Ck = new ArrayList<DataParser.Sequence>();


		//m_Dparser.displaySequence(Fk_minus_1);
//			String s1 = "<(13}{3}>";//			String s2 = "<{22}{3}>";/////			Fk_minus_1.clear();//			Fk_minus_1.add(m_Dparser.toSequence(s1));//			Fk_minus_1.add(m_Dparser.toSequence(s2));


		//join step
		for(int i = 0; i < Fk_minus_1.size(); i++){

			Sequence seq1 = Fk_minus_1.get(i);
			//System.out.print("Sequence 1: ");
			//m_Dparser.displaySequence(seq1);

			for(int j = 0; j < Fk_minus_1.size(); j++){

				Sequence seq2 = Fk_minus_1.get(j);
				//System.out.print("Sequence 2: ");
				//m_Dparser.displaySequence(seq2);
				//System.out.println("------------------");

				//condition one
				if(FitemLessThanOth(seq1)){
					Ck.addAll(doMSCandidateJoin1(seq1, seq2));
				}

				//condition two
				else if(LitemLessThanOth(seq2)){
					Ck.addAll(doMSCandidateJoin2(seq1, seq2));
				}

				else{ //condition three
					Sequence subseq1 = dropItem(seq1, 1);
					Sequence subseq2 = dropItem(seq2, seq2.getLengthOfSequence());
					Item lastItemofseq2 = getItem(seq2, seq2.getLengthOfSequence());					

					//extending s1 with last item of s2 
					if(subseq1.equals(subseq2)){
						//if last item 'l' in s2 is a separate element
						ItemSet lastIS = seq2.getM_ItmSet().get(seq2.getM_ItmSet().size()-1);
						if(lastIS.getM_ItemSet().size() == 1 /*&& lastIS.getM_ItemSet().equals(lastItemofseq2)*/){
							//first candidate sequence
							Sequence c1 = m_Dparser.cloneSequence(seq1);
							ItemSet newis = m_Dparser.new ItemSet();
							newis.addItem(m_Dparser.new Item(lastItemofseq2.getItem()));
							c1.addItmSet(newis);
							//m_Dparser.displaySequence(c1);
							//System.out.println("31");
							//add the candidate sequence
							Ck.add(c1);
						}
						else{

							//second candidate sequence
							Sequence c2 = m_Dparser.cloneSequence(seq1);
							ItemSet oldis = c2.getM_ItmSet().get(c2.getM_ItmSet().size()-1); //last itemset
							oldis.addItem(m_Dparser.new Item(lastItemofseq2.getItem()));
							//m_Dparser.displaySequence(c2);
							//System.out.println("32");
							//add the candidate sequence
							Ck.add(c2);
						}			
					}
				}

			}
		}

		//System.out.println("Ck without pruning..");
		//m_Dparser.displaySequence(Ck);

		//prune the sequence
		//System.out.println("Ck after pruning..");
		List<Sequence> CkAP = pruneSequence(k-1, Ck);
		//m_Dparser.displaySequence(CkAP);

		return CkAP;
	}

	/**
	 * Prunes the sequence of any of there k-1 subsequences are infrequent
	 * @param k_minus_1
	 * @param k_listOfSequence
	 * @return
	 */
	private List<Sequence> pruneSequence(int k_minus_1, List<Sequence> k_listOfSequence){

		//add only valid sequences
		List<Sequence> prunedSequenceList = new ArrayList<DataParser.Sequence>();

		//get all the k_minus_1 sequences
		List<Sequence> Fk_minus_1 = m_listOfall_k_sequences.get(k_minus_1);
		//System.out.println("\nFk-1 sequences..");
		//m_Dparser.displaySequence(Fk_minus_1);


		//		k_listOfSequence.clear();
		//		String str = "<{2}{5}{7}>";
		//		k_listOfSequence.add(m_Dparser.toSequence(str));

		//remove get the itemset from(which will be equivalent to k-1 sequences) from k-sequence candidate list
		for (Sequence ckcandseq : k_listOfSequence) {
			List<Sequence> listOfsubseq = m_Dparser.generatekminus1Sequence(ckcandseq);

			//System.out.println("\nCandidate sequence");
			//m_Dparser.displaySequence(ckcandseq);
			double[] MISofckcandseq = getMISandItemofSequence(ckcandseq);
			Sequence lowest = m_Dparser.toSequence("<{" + String.valueOf((int)MISofckcandseq[0] + "}>"));
			//m_Dparser.displaySequence(lowest);

			//System.out.println("\nList of subsequences..");
			//m_Dparser.displaySequence(listOfsubseq);

			boolean bContainsAll = true;
			for (Sequence subs : listOfsubseq) {

				//m_Dparser.displaySequence(subs);

				//compare with f_k-l
				if(!Fk_minus_1.contains(subs)){
					if(subs.contains(lowest))
						bContainsAll = false;
				}
			}

			//SDC check: Itemset type
			/*List<ItemSet> ckisList = ckcandseq.getM_ItmSet();
			for (ItemSet is : ckisList) {
				int min = new ArrayList<Item>(is.getM_ItemSet()).get(0).getItem();
				double minitmsup = getSupportCount(m_ItmCountMap.get(min), 0, m_Dparser.getM_ListofTransactionSeq().size());
				int max = new ArrayList<Item>(is.getM_ItemSet()).get(is.getM_ItemSet().size()-1).getItem();
				double maxitmsup = getSupportCount(m_ItmCountMap.get(max), 0, m_Dparser.getM_ListofTransactionSeq().size());
				if( Math.abs(minitmsup-maxitmsup) > m_Dparser.getSDC() ){
					bContainsAll = false;
					break;
				}	
			}*/

			//SDC check: sequence type
			int[] min = getLowestSupandItemofSequence(ckcandseq);
			double minitmsup = getSupportCount(m_ItmCountMap.get((int)min[0]), 0, m_Dparser.getM_ListofTransactionSeq().size());
			int[] max = getHighestSupandItemofSequence(ckcandseq);
			double maxitmsup = getSupportCount(m_ItmCountMap.get((int)max[0]), 0, m_Dparser.getM_ListofTransactionSeq().size());
			float SDC = (float) m_Dparser.getSDC();
			float supdiff = (float)Math.abs(minitmsup-maxitmsup);
			if( supdiff > SDC ){
				bContainsAll = false;	
			}

			//add to the sequence only if 'bContainsAll' is true
			if(bContainsAll){
				prunedSequenceList.add(ckcandseq);
			}
		}

		return prunedSequenceList;
	}

	/**
	 * To check the containment of Ck candidate sequences in transaction sequences
	 * @param k
	 * @param CkList
	 * @return
	 */
	private List<Sequence> validateCandidateSeqsinTrans(int k, List<Sequence> CkList){
		List<Sequence> Fk = new ArrayList<DataParser.Sequence>();

		//transaction sequences
		List<Sequence> transactionSeqList = m_Dparser.getM_ListofTransactionSeq();
		HashMap<String, Integer> seqCountMap = new LinkedHashMap<String, Integer>();

		//using list because it does maintain order in iteration
		List<String> ckstrList = new ArrayList<String>();

		//System.out.println("In validation..");
		//m_Dparser.displaySequence(CkList);

		//transaction sequence containment check
		for (Sequence ck : CkList ) {
			//System.out.println("Transaction sequence");
			//m_Dparser.displaySequence(tranSeq);
			//System.out.println("Contained candidate sequences");
			for (Sequence tranSeq : transactionSeqList) {
				if(tranSeq.contains(ck)){
					//m_Dparser.displaySequence(ck);
					//construct a string
					String ckstr = m_Dparser.toString(ck);
					if(seqCountMap.containsKey(ckstr)){
						seqCountMap.put(ckstr, seqCountMap.get(ckstr)+1);
						//System.out.println(ckstr + " count: " + m_SeqCount.get(ckstr));
					}
					else{
						seqCountMap.put(ckstr, 1);
						ckstrList.add(ckstr);
						//System.out.println(ckstr);
					}
				}
			}
		}

		//check the minsup
		double n = m_Dparser.getM_ListofTransactionSeq().size();

		String displayString = new String();
		for (String seqstr : ckstrList) {
			Sequence seq = m_Dparser.toSequence(seqstr);
			double MISofSeq[] = getMISandItemofSequence(seq);

			//check for valid
			if(MISofSeq[1] == INVALIDMISVALUE)
				continue;

			//check for Sequence minsup condition
			double seqCount = seqCountMap.get(seqstr);
			if( seqCount/n >= MISofSeq[1]){
				// k-sequential pattern list
				displayString += "Pattern: " + seqstr + " Count: " + (int)seqCount + "\n";
				Fk.add(seq);
			}
		}

		writeOutput("The number of length " + k + " sequential patterns is " + Fk.size() + "\n");
		writeOutput(displayString + "\n");

		//insert Fk sequences
		m_listOfall_k_sequences.put(k, Fk);
		return Fk;
	}

	private void generateFk(){
		List<Integer> L = initpass();

		writeOutput("The number of length " + 1 + " sequential patterns is ");
		List<Sequence> F1 = generateF1(L);
		List<Sequence> Fk_minus_1 = F1;
		//insert F1
		HashMap<Integer, List<Sequence>> FkMap = new LinkedHashMap<Integer, List<Sequence>>();
		FkMap.put(1, F1);

		List<Sequence> ck = null;
		for(int k = 2; !Fk_minus_1.isEmpty(); k++){
			if( k == 2)
				ck = level2CandidateGenSPM(L); //c2
			else
				ck = MSCandidateGenSPM(k, Fk_minus_1);

			//generate Fk
			List<Sequence> Fk = validateCandidateSeqsinTrans(k, ck);

			//iterate for next
			Fk_minus_1.clear();
			Fk_minus_1 = Fk;
		}
	}

	private void writeOutput(String data){

		if(m_OutputFileName.isEmpty())
			System.out.println(data);
		else{
			try {
				File file = new File(m_OutputFileName);
				BufferedWriter out = new BufferedWriter( new FileWriter(file, true));
				out.write(data);
				out.close();
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {	
		/*MSGSP msgsp = new MSGSP("./data/data.txt", "./data/para.txt", "./output.txt");
		msgsp.generateFk();*/

		/*MSGSP msgsp = new MSGSP("./data/data/1/dataTest.txt", "./data/data/1/paraTest.txt", "./output1.txt");
		msgsp.generateFk();

		msgsp = new MSGSP("./data/data/2/dataTest.txt", "./data/data/2/paraTest.txt", "./output2.txt");
		msgsp.generateFk();

		msgsp = new MSGSP("./data/data/3/dataTest.txt", "./data/data/3/paraTest.txt", "./output3.txt");
		msgsp.generateFk();

		msgsp = new MSGSP("./data/data/4/dataTest.txt", "./data/data/4/paraTest.txt", "./output4.txt");
		msgsp.generateFk();

		MSGSP msgsp = new MSGSP("./data/data/5/dataTest.txt", "./data/data/5/paraTest.txt", "./output5.txt");
		msgsp.generateFk();*/

		MSGSP msgsp = new MSGSP("./data/test-data/small-data-1/data-1.txt", "./data/test-data/small-data-1/para1-1.txt", 
		"./data/test-data/result1-1.txt");
		msgsp.generateFk();

		msgsp = new MSGSP("./data/test-data/small-data-1/data-1.txt", "./data/test-data/small-data-1/para1-2.txt", 
		"./data/test-data/result1-2.txt");
		msgsp.generateFk();

		msgsp = new MSGSP("./data/test-data/large-data-2/data2.txt", "./data/test-data/large-data-2/para2-1.txt", 
		"./data/test-data/result2-1.txt");
		msgsp.generateFk();

		msgsp = new MSGSP("./data/test-data/large-data-2/data2.txt", "./data/test-data/large-data-2/para2-2.txt", 
		"./data/test-data/result2-2.txt");
		msgsp.generateFk();
	}
}
